var express = require('express');
var router = express.Router();
var createError = require('http-errors');

var moment = require('moment');
var passport = require('passport');


function CreateQuery(req, assign){
  let campus = req.params.campus;
  let orgTree = req.app.locals.orgTree;
  let scope = req.user.scope;
  
  var campusId = orgTree.getId(campus);
  if(!scope.includes(campusId)){
    campusId = scope;
  };
  
  let pickupFilter = {"pickup_lib": campusId };

  let query  = {
    "cancel_time":null,
    "pickup_time":null,
    "return_time":null,
    "capture_time":null,
  };

  return Object.assign({},query,pickupFilter,assign);
}

let restrict = function(req, res, next){
  if (!req.isAuthenticated()) {
    req.session.redirectTo = req.baseUrl;
    res.redirect(app.locals.base_path + '/login');
  } else {
    next();
  }
};

// ROOT URL
router.get('/', restrict, function(req, res, next) {

    let orgTree = req.app.locals.orgTree;

    let branches = req.user.scope.map( (id) => {
      let sn = orgTree.getShortname(id);
      let tsn = sn.split("-")[1] || sn;
      return { 
        "shortname": sn,
        "display_name": tsn
      }
    });

    res.render('bookings/index', {"branches":branches});
});

// REMAPS A BRESV OBJECT TO A HUMAN FRIENDLY OBJECT
let remapBresv = (bresv, orgTree) => {
  var result = {}

  let timefields = ["start_time","end_time","return_time","capture_time","pickup_time","cancel_time"];
  timefields.forEach( (t) => {
    time = eval('bresv.' + t + '()');
    if(time){
      d = new Date(time);
      result[t] = d.toLocaleString();
    }
  })

  result["booking_id"] = bresv.id();

  result["request_library"] = orgTree.getShortname( bresv.request_lib() );
  result["pickup_library"] = orgTree.getShortname( bresv.pickup_lib() );

  result["patron_barcode"] = bresv.usr().card().barcode();
  result["patron_id"] = bresv.usr().id();
  result["patron_lname"] = bresv.usr().family_name();

  if(bresv.current_resource()){
    result["current_barcode"]= bresv.current_resource().barcode();
  }
  if(bresv.target_resource_type()){
    result["target_type"] = bresv.target_resource_type().name();
  } 
  if(bresv.target_resource()) {
    result["target_barcode"] = bresv.target_resource().barcode();
  }
  if(bresv.capture_staff()) {
    result["capture_staff"] = bresv.capture_staff()//.usrname();
  }

  // state
  if(bresv.cancel_time()){
    result["state"] = "cancelled";
  } else if (bresv.return_time()){
    result["state"] = "returned";
  } else if (bresv.pickup_time()){
    if( moment(bresv.end_time()) < moment() ){
      result["state"] = "overdue"
    } else {
      result["state"] = "pickedup";
    }
  } else if(bresv.capture_time()){
    result["state"] = "captured";
  } else {
    result["state"] = "new";
  }
  return result;
}

let getResourceByBarcode = function(req){
  let conn = req.app.locals.conn;
  let authToken = req.session.passport.user;
  let query = {"barcode":req.query.barcode};
  let pcrud = conn.createSession("open-ils.pcrud")
  return pcrud.requestPromise(
    {"method":"open-ils.pcrud.search.brsrc", "params":[authToken, query]})
}

// Given a list (bookingList) of booking object
// do a pcrud search of acp objects for the current_barcodes
// return a promise
let fleshCurrentItemBarcodes = function(req, bookingList){
  if(bookingList.length == 0 ){
    return Promise.resolve(bookingList);
  }
  let conn = req.app.locals.conn;
  let authToken = req.session.passport.user;

  // get an array of barcodes only
  let barcodes = bookingList.map( (b) => b.current_barcode ).filter( (b) => b != undefined );
  if(barcodes.length == 0){
    return Promise.resolve(bookingList);
  }
  let query = {
    "barcode": barcodes
  }
  let flesh = {
    "flesh":2,
    "flesh_fields":{
      "acp":["status","call_number"]
    }
  }
  let pcrud = conn.createSession("open-ils.pcrud")
  let bc = pcrud.request(
    {"method":"open-ils.pcrud.search.acp", "params":[authToken, query, flesh]})

  // map of barcodes to acp objects
  let acpMap = {};

  return new Promise( (resolve, reject) => {
    bc.on("response", (acp) => acpMap[acp.barcode()] = acp );
    bc.on("complete", (r) => {
      let final = bookingList.map( (b) => {
        // try to get the acp from out hash
        let acp = acpMap[b.current_barcode];
        // if we find it, flesh it out
        if(acp){
          b["call_number"] = acp.call_number().label();
          b["copy_number"] = acp.copy_number();
          b["status"] = acp.status().name();
        }
        return b;
      });
      resolve(final);
    });
    bc.on('error', (e,f,g) => reject(g) );
    bc.on('methoderror', (e,f,g) => reject(g) );
  });
}
// Runs the basic Pcrud query for bresv objects
let doQuery = function(req, query){
  let conn = req.app.locals.conn;
  let orgTree = req.app.locals.orgTree;
  let authToken = req.user.authtoken
  let myQuery = Object.assign(query, {"request_lib": req.user.scope });

  let flesh = {
    "flesh":3,
    "flesh_fields":{
      "bresv":["current_resource","target_resource","target_resource_type","usr"],
      "au":["card"],
      "brsrc":["brt"]
    },
    "order_by":[
      {"class":"bresv","field":"start_time"},
      {"class":"bresv","field":"end_time"}
    ]
  };

  let finalList = [];
  
  var pcrud = conn.createSession("open-ils.pcrud")
  return pcrud.request(
    {"method":"open-ils.pcrud.search.bresv", "params":[authToken, myQuery, flesh]})
}


// create pcrud query objects for predefined date intervals
let createDateRange = function(interval,key){
  let st = [];
  switch(interval){
    case "today":
      st = {"between": [
        moment().startOf('day').toISOString(),
        moment().endOf('day').toISOString()
      ]}
      break;
    case "tomorrow":
      st = {"between":[
        moment().startOf('day').add(1,"day").toISOString(),
        moment().endOf('day').add(1,"day").toISOString()
      ]}
      break;
    case "next3days":
      st = {"between":[
        moment().startOf('day').toISOString(),
        moment().endOf('day').add(3,"days").toISOString()
      ]}
      break;
    case "lastmonth":
        st = {"between":[
          moment().endOf('day').subtract(1,"month").toISOString(),
          moment().endOf('day').toISOString()
        ]}
        break;
    case "lastweek":
        st = {"between":[
          moment().endOf('day').subtract(1,"week").toISOString(),
          moment().endOf('day').toISOString()
        ]}
        break;
    case "last3months":
        st = {"between":[
          moment().endOf('day').subtract(3,"months").toISOString(),
          moment().endOf('day').toISOString()
        ]}
        break;
    case "all":
      st = null;
      break;
  }
  var res = {}
  if(st){
    res[key] = st;
  }
  return res;
}

// gets all uncancelled, uncaptured
// bookings with start time within :interval (see createDeateRange)
// for campus :campus
router.get('/open/:interval/:campus', restrict, function(req, res, next) {
  var fail = (code) => {next(createError(code))};


  var query = CreateQuery(req,{});

  let interval = req.params.interval;

  let range = createDateRange(interval,"start_time");

  query = Object.assign(query, range);

  var p = doQuery(req, query)
  
  var openBookings = [];
  
  p.on("response", (r) => {
    let b = remapBresv(r,req.app.locals.orgTree);
    openBookings.push(b);
  })
  p.on("complete", (e) => {
    fleshCurrentItemBarcodes(req, openBookings)
      .then( (finalBookings) => {
        res.render("bookings/open", { bookings: finalBookings })
      })
      .catch( () => fail(500) )
  })
  p.on("error", () => fail(404));
});


// gets all captured
// bookings with start time within :interval (see createDeateRange)
// for campus :campus
router.get('/captured/:campus', restrict, function(req, res, next) {
  var fail = (code) => {next(createError(code))};

  var query = CreateQuery(req, {"capture_time":{"!=":null}} );

  var p = doQuery(req, query)

  var capturedBookings = [];
  
  p.on("response", (r) => {
    let b = remapBresv(r,req.app.locals.orgTree);
    capturedBookings.push(b);
  })
  p.on("complete", (e) => {
    fleshCurrentItemBarcodes(req, capturedBookings)
      .then( (finalBookings) => {
        res.render("bookings/captured", { bookings: finalBookings })
      })
      .catch( () => fail(500) )
  })
  p.on("error", () => fail(404));
});

router.get('/out/:campus', restrict, function(req, res, next) {
  var fail = (code) => {next(createError(code))};

  var query = CreateQuery(req, {"capture_time":{"!=":null},"pickup_time":{"!=":null}} );

  var p = doQuery(req, query)

  var outBookings = [];
  
  p.on("response", (r) => {
    let b = remapBresv(r,req.app.locals.orgTree);
    outBookings.push(b);
  })
  p.on("complete", (e) => {
    fleshCurrentItemBarcodes(req, outBookings)
      .then( (finalBookings) => {
        res.render("bookings/out", { bookings: finalBookings })
      })
      .catch( () => fail(500) )
  })
  p.on("error", () => fail(404));
});


router.get('/cancelled/:interval/:campus', restrict, function(req, res, next) {
  var fail = (code) => {next(createError(code))};

  var query = CreateQuery(req,{});

  let interval = req.params.interval;

  let range = createDateRange(interval,"cancel_time");

  query = Object.assign(query, range, {"cancel_time":{"!=":null}});

  var p = doQuery(req, query)
  
  var cancelledBookings = [];
  var fail = (code) => {next(createError(code))};
  
  p.on("response", (r) => {
    let b = remapBresv(r,req.app.locals.orgTree);
    cancelledBookings.push(b);
  })
  p.on("complete", (e) => {
    fleshCurrentItemBarcodes(req, cancelledBookings)
      .then( (finalBookings) => {
        res.render("bookings/cancelled", { bookings: finalBookings })
      })
      .catch( () => fail(500) )
  })
  p.on("error", () => fail(404));
});

router.get('/item', restrict, function(req, res, next) {
  var fail = (code) => { return next(createError(code))};

  getResourceByBarcode(req)
    .then( (brsrcs) => {
      if(brsrcs.length == 1){
        let brsrc = brsrcs.shift();
        var query = {
          "pickup_lib":req.user.scope,
          "current_resource":brsrc.id()
        }
        var p = doQuery(req, query)
        var itemBookings = []
        p.on("response", (r) => {
          let b = remapBresv(r,req.app.locals.orgTree);
          itemBookings.push(b);
        })
        p.on("complete", (e) => {
          res.render("bookings/item", { bookings: itemBookings })
        })
        p.on("error", () => fail(404));
      } else {
        fail(404)
      }
    })
    .catch( (e,f,g) => {
      fail(500);
    });
});




module.exports = router;
