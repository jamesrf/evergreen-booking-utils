


var OrgTree = function(conn, opts){
    this.conn = conn;
    this.org_tree = [];
    this.idToShortname = {};
    this.shortnameToId = {};
    this.aous = {};
    this.ouTypeMap = {};
}

OrgTree.prototype.init = async function(){
    var self = this;
    console.log("Building Org Tree...");
    await self.conn.connect();

    let actor = self.conn.createSession("open-ils.actor");

    let promiseOuTypes = actor.requestPromise("open-ils.actor.org_types.retrieve")

    let promiseOrgTree = actor.requestPromise("open-ils.actor.org_tree.retrieve")

    return new Promise( (resolve, reject) => {
        var p = Promise.all([promiseOuTypes, promiseOrgTree])
        p.then( (results) => {
            let ouTypes = results[0][0]
            self.ouTypeMap = ouTypes.reduce( (m,v) => {
                m[v.id()] = v;
                return m;
            }, {})
            
            let orgTree = results[1][0]
            let processTreeChild = function(c){
                self.shortnameToId[ c.shortname() ] = c.id();
                self.aous[ c.id() ] = c;
                c.children().forEach( processTreeChild )
            }
            processTreeChild(orgTree);
            self.org_tree = orgTree;
            console.log("Org Tree Built");
            resolve()
        })
        .catch(reject);
        
    });
    
}

OrgTree.prototype.getShortname = function(id){
    return this.aous[parseInt(id)].shortname();
}
OrgTree.prototype.getId = function(shortname){
    return this.shortnameToId[shortname];
}

// FIXME: this is not ideal
OrgTree.prototype.getProbableWorkLocations = function(id){
    let aou = this.aous[ id ];
    let aout = this.ouTypeMap[ aou.ou_type() ]
    if(aout.depth == this.scopeDepth){
        return [aou.id()]
    }
    let parent = this.aous[aou.parent_ou()]
    if(!parent){
        parent = aou;
    }
    let children = parent.children();
    if(!children){
        return [aou.id()];
    }
    return children.map( (x) => x.id() );
}

module.exports = OrgTree;