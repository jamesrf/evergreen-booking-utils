const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const fs = require('fs');

// auth middleware
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

// osrf client 
const osrf = require('opensrf');

// routes
const indexRouter = require('./routes/index');
const bookingsRouter = require('./routes/bookings');

const EGCOOKIE = 'egcookie';

let BASE_PATH = process.env.BASE_PATH || '';

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));

let rawOpts = fs.readFileSync('config.json');
let config = JSON.parse(rawOpts);

app.use(require('express-session')({ name: EGCOOKIE, secret: config.secret, resave: false, saveUninitialized: false }));

var conn = new osrf.Connection(config.osrf);
app.locals.conn = conn;
app.locals.base_path = BASE_PATH;
app.locals.host = config.osrf.host;

var OrgTree = require('./org_tree');
app.locals.orgTree = new OrgTree(conn);
app.locals.orgTree.init();

// EVERGREEN AUTH SETUP
app.use(passport.initialize());
app.use(passport.session());

// fetches session and fleshes out to a useful session object
// containing user, scope orgs and the token
let fetchSession = function(authtoken, done){
  let auth = conn.createSession("open-ils.auth")
  auth.requestPromise({"method":"open-ils.auth.session.retrieve", "params":[authtoken]})
    .then( (aus) => {
      au = aus[0]
      if(au['textcode'] == "NO_SESSION"){
          done(au,authtoken)
      } else {
          let orgs = app.locals.orgTree.getProbableWorkLocations(au.home_ou());
          let user = {"user":au,"authtoken":authtoken,"scope":orgs}
          done(null, user)
      }
    })
    .catch( (err) => {
      done(err, authtoken);
    })
}
passport.use(new LocalStrategy(
  function(username, password, cb) {
    conn.login(username, password)
        .then( (authtoken) => {
          fetchSession(authtoken, cb);
        })
        .catch( (e) => {
          console.error(e);
          cb(null, false, {"message":"Login failed"});
        });
  }
));
passport.serializeUser( (user, done) => {
  done(null, user.authtoken);
});

passport.deserializeUser( (authtoken,done) => {
  fetchSession(authtoken, done);
});

app.use('/', indexRouter);


let restrict = function(req, res, next){
  if (!req.session.passport || !req.isAuthenticated()) {
    req.session.redirectTo = BASE_PATH + req.baseUrl;
    res.redirect(BASE_PATH + '/login');
  } else {
    next();
  }
};

app.use('/bookings', restrict, bookingsRouter);

app.get('/login',
  function(req, res){
    res.render('login');
  }
);
app.post('/login',
  passport.authenticate('local',  { 
    failureRedirect: BASE_PATH + '/login/fail' }),
  function(req,res){
    if(req.session.redirectTo == undefined){
        res.redirect(BASE_PATH);
    } else {
        res.redirect(req.session.redirectTo);
    }
  }
);
app.get('/logout',
  function(req, res){
    req.logout();
    req.session.destroy()
    res.clearCookie(EGCOOKIE, {path:'/'}).status(200).redirect(BASE_PATH );
});
app.get('/login/fail',
  function(req, res){
    res.render('loginfail');
  }
);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  // next(createError(err.status || 500));

  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
